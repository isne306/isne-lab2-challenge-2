﻿#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head, 0);
	if(tail==0)
	{
		tail = head;
	}
	else
	{	
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	Node *p = new Node(el);
	if(isEmpty())
	{
		head = tail=p;
	}
	else
	{
		tail->next = p;
		p->prev = tail;
		tail = tail->next;
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	if(isEmpty())
	{
		return -1;
	}
	else
	{
		if(head==tail)
		{
			int p;
			Node *tmp = head;
			p = head->data;
			delete tmp;
			head = NULL;
			tail = NULL;
			return p;
		}
		else
		{
			int q;
			Node *tmp = tail;
			tmp = tmp->prev;
			q = tail->data;
			delete tail;
			tmp->next = NULL;
			tail = tmp;
			return q;
		}
	}
}
bool List::search(char el) 	// TO DO! (Function to return True or False depending if a character is in the list. //return NULL;
{
	Node *tmp = head;
	while(tmp != NULL)
	{
		if(tmp->data == el )
		{
			return true;
		}
		else
		{
			tmp = tmp->next;
		}
	}
	return false;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
