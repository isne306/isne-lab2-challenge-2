﻿#include <iostream>
#include "list.h"
using namespace std;

int main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('n');
	mylist.pushToHead('e');
	mylist.pushToHead('k');
	cout<< "Push To Head: " <<endl;
	mylist.print();
	cout << endl;
	cout << endl;

	mylist.pushToTail('a');
	mylist.pushToTail('r');
	mylist.pushToTail('t');
	cout<< "Push To Tail: " <<endl;
	mylist.print();
	cout << endl;
	cout << endl;

	cout<< "Pop from head: " << mylist.popHead() << endl;
	mylist.print();
	cout << endl;
	cout << endl;

	cout<< "Pop from tail: " << mylist.popTail() << endl;
	mylist.print();
	cout << endl;
	cout << endl;

	cout<< "Search 'r' 'a' 'p'in List: " <<endl;
	if(mylist.search('r'))
	{
     	cout << "Found 'r'"<< endl;
	}
	if(mylist.search('a'))
	{
     	cout << "Found 'a'" << endl;
	}
	if(mylist.search('p'))
	{
		cout << "Found 'p'"<< endl;
	}
	else
	{
		cout << "Not found 'p'"<< endl;
	}

	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?
	
}